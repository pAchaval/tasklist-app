require('./models/task');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const taskRoutes = require('./routes/taskRoutes');

const app = express();

app.use(express.json());
app.use(cors());

app.use('/tasks', taskRoutes);

const mongoUri = 'mongodb+srv://admin:iberaibera@cluster0.u8eqq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
mongoose.connect(mongoUri);

mongoose.connection.on('connected', () => {
    console.log('connected to mongo instance');
});

mongoose.connection.on('error', (err) => {
    console.error('Error connecting to mongo', err);
})

app.get('/', (req, res) => {
    res.send('Hi there!');
});

app.listen(3001, () => {
    console.log('listeninig on port 3001');
})
