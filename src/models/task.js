const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    title: {
        type: String,
        unique: true,
        required: true,
    }
});

mongoose.model('Task', taskSchema);
