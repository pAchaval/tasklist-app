const express = require('express');
const fetch = require('node-fetch');
const mongoose = require('mongoose');
const Task = mongoose.model('Task');

const router = express.Router();

router.get(
    '/',
    fetchTitles,
    getSavedTasks,
)

router.put(
    '/',
    async (req, res) => {
        const { id } = req.query;
        try {
            await Task.findByIdAndDelete(id)
            res.status(200).send('Task completed')
        } catch (err) {
            res.status(404).send('Error completing task');
        }
    }
)

router.get(
    '/remove',
    removeTasks,
    getSavedTasks
)

async function removeTasks(req, res, next) {
    try {
        await Task.deleteMany({});
        res.status(200).send('Db was wiped');
    } catch (err) {
        res.status(404).send('Error deleting collection');
    }
}

async function fetchTitles(req, res, next) {
    
    const { numberOfTasks } = req.query;
    if (numberOfTasks === undefined) next();

    const api_url = `https://lorem-faker.vercel.app/api?quantity=${numberOfTasks}`;
    const fetch_response = await fetch(api_url);
    const json = await fetch_response.json();

    json.map(async (title) => {
        try {
            const task = new Task({ title });
            await task.save();
        } catch (err) {
            res.status(404).send('Something went wrong fetching titles');
            next(err);
        }
    });

    next();
}

async function getSavedTasks(req, res, next) {
    
    const { savedTasks } = req.query;
    const query = savedTasks !== undefined ? Task.find().limit(parseInt(savedTasks)) : Task.find();

    try {
        const tasks = await query;
        res.locals.tasks = tasks;
        res.send(tasks);
    } catch {
        res.status(404).send('Somethin went wrong getting tasks');
    }

}

module.exports = router;
